package io.bitcode.eventu;

import java.lang.invoke.MethodHandle;
import java.util.List;

public interface ExecutionHandler {
    void fireEvent(Object handlerInstance, MethodHandle method, AbstractEvent event);

    List<MethodHandle> determineMethodHandles(ClassRepository repository, Class<? extends AbstractEvent> eventClass);
}
