package io.bitcode.eventu.impl;

import io.bitcode.eventu.AbstractEvent;
import io.bitcode.eventu.ClassRepository;
import io.bitcode.eventu.Eventu;
import io.bitcode.eventu.RegistrationHandler;
import io.bitcode.eventu.annot.EventHandler;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultRegistrationHandler implements RegistrationHandler {

    @Override
    public void handleRegistration(ClassRepository repository, Object instance) {
        if (isRegistered(repository, instance)) {
            return;
        }

        if (Eventu.get().getClassCache().containsKey(instance.getClass())) {
            List<Method> methods = Eventu.get().getClassCache().get(instance.getClass());

            for (Method method : methods) {
                try {
                    Class<? extends AbstractEvent> eventType = findEventType(method, instance);
                    method.setAccessible(true);
                    MethodHandle unreflectMethodHandle = MethodHandles.lookup().unreflect(method);
                    repository.addHandleForEvent(eventType, unreflectMethodHandle);
                    repository.addInstanceForEvent(eventType, instance);
                    repository.addInstanceForMethodHandle(unreflectMethodHandle, instance);
                } catch (IllegalAccessException e) {
                    System.err.println("Method will not be fired: " + e.getMessage());
                }
            }
        } else {
            Eventu.get().getClassCache().put(instance.getClass(),
                    Arrays.stream(instance.getClass().getDeclaredMethods())
                            .filter(m -> m.isAnnotationPresent(EventHandler.class)).collect(Collectors.toList()));
            handleRegistration(repository, instance);
        }
    }

    private Class<? extends AbstractEvent> findEventType(Method method, Object instance) {

        Class<?>[] types = method.getParameterTypes();
        if (types.length < 1) {
            throw new IllegalStateException("An event handler method must have the event type as first parameter!");
        }

        if (AbstractEvent.class.isAssignableFrom(types[0])) {
            return (Class<? extends AbstractEvent>) types[0];
        } else {
            throw new IllegalStateException("The first parameter of method handler is not an event!");
        }
    }

    @Override
    public boolean isRegistered(ClassRepository repository, Object instance) {
        return repository.isInstanceRegistered(instance);
    }
}
