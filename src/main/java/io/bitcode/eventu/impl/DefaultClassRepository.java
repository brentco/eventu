package io.bitcode.eventu.impl;

import io.bitcode.eventu.AbstractEvent;
import io.bitcode.eventu.ClassRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.invoke.MethodHandle;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
public class DefaultClassRepository implements ClassRepository {

    private final Logger logger = LogManager.getLogger(this);

    private Map<Class<? extends AbstractEvent>, List<WeakReference>> instancesByEventMap = new HashMap<>();
    private Map<Class<? extends AbstractEvent>, List<MethodHandle>> methodHandlesByEventMap = new HashMap<>();

    private Map<MethodHandle, List<WeakReference>> instancesByMethodHandleMap = new HashMap<>();

    /**
     * Adds an instance that handles the given type of event.
     *
     * @param eventClass The event it will be registered for.
     * @param instance   The instance to be registered.
     */
    @Override
    public void addInstanceForEvent(Class<? extends AbstractEvent> eventClass, Object instance) {
        List<WeakReference> weakReferences;

        if (instancesByEventMap.containsKey(eventClass)) {
            weakReferences = instancesByEventMap.get(eventClass);
            logger.debug("Retrieving cached instance list for event '{}'", eventClass.getSimpleName());
        } else {
            weakReferences = new ArrayList<>();
            instancesByEventMap.put(eventClass, weakReferences);
            logger.debug("Creating new instance list for event '{}'", eventClass.getSimpleName());

        }

        if (weakReferences.contains(new WeakReference(instance))) {
            logger.debug("Instance already registered for event '{}'", eventClass.getSimpleName());
            return;
        }

        weakReferences.add(new WeakReference(instance));
        logger.debug("Registered new instance for event '{}'", eventClass.getSimpleName());
    }

    @Override
    public void addInstanceForMethodHandle(MethodHandle handle, Object instance) {
        List<WeakReference> weakReferences;

        if (instancesByMethodHandleMap.containsKey(handle)) {
            weakReferences = instancesByMethodHandleMap.get(handle);
        } else {
            weakReferences = new ArrayList<>();
            instancesByMethodHandleMap.put(handle, weakReferences);

        }

        if (weakReferences.contains(new WeakReference(instance))) {
            return;
        }

        weakReferences.add(new WeakReference(instance));
    }

    /**
     * Adds a method handle for a given type of event.
     *
     * @param eventClass The event type the method handle will be registered for.
     * @param handle     The method handle to be registered.
     */
    @Override
    public void addHandleForEvent(Class<? extends AbstractEvent> eventClass, MethodHandle handle) {
        List<MethodHandle> methodHandles;

        if (instancesByEventMap.containsKey(eventClass)) {
            methodHandles = methodHandlesByEventMap.get(eventClass);
        } else {
            methodHandles = new ArrayList<>();
            methodHandlesByEventMap.put(eventClass, methodHandles);
        }

        if (methodHandles.contains(handle)) {
            return;
        }

        methodHandles.add(handle);
    }

    @Override
    public boolean isInstanceRegistered(Object instance) {
        return instancesByEventMap.values().stream().anyMatch(l -> l.contains(new WeakReference(instance)));
    }

    /**
     * @param instance
     * @return all method handles that can be called for a given instance.
     */
    @Override
    public List<MethodHandle> getMethodHandlesForInstance(Object instance) {

        List<MethodHandle> methodHandles = new ArrayList<>();
        for (Map.Entry<MethodHandle, List<WeakReference>> methodHandleListEntry : instancesByMethodHandleMap.entrySet()) {
            if (methodHandleListEntry.getValue().contains(new WeakReference(instance))) {
                methodHandles.add(methodHandleListEntry.getKey());
            }
        }

        return methodHandles;
    }

    /**
     * @param eventClass
     * @return all method handles that can be called for a given event.
     */
    @Override
    public List<MethodHandle> getMethodHandlesForEvent(Class<? extends AbstractEvent> eventClass) {
        return methodHandlesByEventMap.getOrDefault(eventClass, new ArrayList<>());
    }

    /**
     * @param eventClass
     * @return all instances that can treat the given event.
     */
    @Override
    public List<WeakReference> getInstancesForEvent(Class<? extends AbstractEvent> eventClass) {
        return instancesByEventMap.getOrDefault(eventClass, new ArrayList<>());
    }

    /**
     * @param methodHandle
     * @return all instances that can be used for the given method handle.
     */
    @Override
    public List<WeakReference> getInstancesForMethodHandle(MethodHandle methodHandle) {
        return instancesByMethodHandleMap.getOrDefault(methodHandle, new ArrayList<>());
    }
}
