package io.bitcode.eventu.impl;

import io.bitcode.eventu.AbstractEvent;

public class BasicEvent implements AbstractEvent {

    private boolean cancelled;

    @Override
    public void cancel() {
        cancelled = true;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }
}
