package io.bitcode.eventu.impl;

import io.bitcode.eventu.AbstractEvent;
import io.bitcode.eventu.ClassRepository;
import io.bitcode.eventu.ExecutionHandler;

import java.lang.invoke.MethodHandle;
import java.util.List;

public class DefaultExecutionHandler implements ExecutionHandler {

    @Override
    public void fireEvent(Object handlerInstance, MethodHandle method, AbstractEvent event) {
        try {
            method.bindTo(handlerInstance).invoke(event);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public List<MethodHandle> determineMethodHandles(ClassRepository repository, Class<? extends AbstractEvent> eventClass) {
        return repository.getMethodHandlesForEvent(eventClass);
    }
}
