package io.bitcode.eventu;


import java.lang.invoke.MethodHandle;
import java.lang.ref.WeakReference;
import java.util.List;

public interface ClassRepository {


    /**
     * Adds an instance that handles the given type of event.
     * @param eventClass The event it will be registered for.
     * @param instance The instance to be registered.
     */
    void addInstanceForEvent(Class<? extends AbstractEvent> eventClass, Object instance);

    /**
     * Adds a method handle for a given type of event.
     * @param eventClass The event type the method handle will be registered for.
     * @param handle The method handle to be registered.
     */
    void addHandleForEvent(Class<? extends AbstractEvent> eventClass, MethodHandle handle);

    void addInstanceForMethodHandle(MethodHandle handle, Object instance);

    boolean isInstanceRegistered(Object instance);

    /**
     *
     * @param instance
     * @return all method handles that can be called for a given instance.
     */
    List<MethodHandle> getMethodHandlesForInstance(Object instance);

    /**
     *
     * @param eventClass
     * @return all method handles that can be called for a given event.
     */
    List<MethodHandle> getMethodHandlesForEvent(Class<? extends AbstractEvent> eventClass);

    /**
     *
     * @param eventClass
     * @return all instances that can treat the given event.
     */
    List<WeakReference> getInstancesForEvent(Class<? extends AbstractEvent> eventClass);

    /**
     *
     * @param methodHandle
     * @return all instances that can be used for the given method handle.
     */
    List<WeakReference> getInstancesForMethodHandle(MethodHandle methodHandle);

}
