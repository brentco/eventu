package io.bitcode.eventu;

import io.bitcode.eventu.aspect.InstanceRegistrationAspect;
import io.bitcode.eventu.impl.DefaultClassRepository;
import io.bitcode.eventu.impl.DefaultExecutionHandler;
import io.bitcode.eventu.impl.DefaultRegistrationHandler;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.lang.invoke.MethodHandle;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.*;

/**
 * The main entry point of Eventu. All your event processing <i>shall pass</i> through here.
 */
public final class Eventu {

    private static final Eventu instance = new Eventu();

    /**
     * Used by the {@link InstanceRegistrationAspect} to cache results of scans.
     * This reduces performance decrease dramatically. Yay for caching! :-)
     */
    private final Map<Class<?>, Boolean> scanResultCache = new HashMap<>();

    /**
     * Used to cache the methods annotated with {@link io.bitcode.eventu.annot.EventHandler}.
     */
    private final Map<Class<?>, List<Method>> classCache = new HashMap<>();
    private final ClassRepository repository = new DefaultClassRepository();
    private RegistrationHandler registrationHandler;
    private ExecutionHandler executionHandler;
    /**
     * Used while no {@link RegistrationHandler} is available.
     * Gets processed entirely when one is set using the {@link #setRegistrationHandler(RegistrationHandler)} method.
     */
    private Queue<Object> registrationQueue = new ArrayDeque<>();

    private Queue<AbstractEvent> executionQueue = new ArrayDeque<>();


    /**
     * Instantiate instantiate, you can not;
     * <p>
     * Creating me again, you should not.
     */
    private Eventu() {

    }

    /**
     * Initializes Eventu. This exists because objects may register before you have the possibility to add
     * your own {@link RegistrationHandler}. Objects will be placed in a registration queue
     * until {@link Eventu#setRegistrationHandler(RegistrationHandler)} or this method is called.
     * <p>
     * This method will call {@link Eventu#setRegistrationHandler(RegistrationHandler)} if no registration handler is set.
     */
    public static void initialize() {
        if (instance.registrationHandler == null)
            setRegistrationHandler(new DefaultRegistrationHandler());

        if(instance.executionHandler == null)
            setExecutionHandler(new DefaultExecutionHandler());
    }

    /**
     * Registers an object for event handling. If no {@link RegistrationHandler} is specified, they will be placed into
     * a queue until one is specified.
     *
     * @param o The object to register.
     * @see #initialize()
     */
    public static void register(Object o) {
        if (instance.registrationHandler == null) {
            instance.registrationQueue.add(o);
        } else {
            instance.registrationHandler.handleRegistration(instance.repository, o);
        }
    }

    /**
     * Sets the {@link RegistrationHandler} used for processing handler instances. When the {@link #registrationQueue}
     * is not empty, all the entries are processed using the newly set {@code Registration Handler}.
     *
     * @param handler The handler to set for processing handler instance registrations.
     */
    @Contract("null -> fail")
    public static void setRegistrationHandler(@NotNull RegistrationHandler handler) {
        if (handler == null) {
            throw new NullPointerException("Registrationhandler must not be null!");
        }

        instance.registrationHandler = handler;
        if (instance.registrationQueue.isEmpty()) {
            return;
        }

        instance.registrationQueue.forEach(instance -> get().registrationHandler.handleRegistration(Eventu.instance.repository, instance));
    }

    public static void setExecutionHandler(@NotNull ExecutionHandler handler) {
        if (handler == null) {
            throw new NullPointerException("Executionhandler must not be null!");
        }

        instance.executionHandler = handler;
        if (instance.executionQueue.isEmpty()) {
            return;
        }

        instance.executionQueue.forEach(event -> fire(event));
    }

    public static boolean isRegistered(Object o) {
        return instance.registrationHandler != null && instance.registrationHandler.isRegistered(instance.repository, o);
    }

    /**
     * This accessor is used to access the internal Eventu systems. Not required for normal operation.
     *
     * @return The Eventu instance for hacking and breaking.
     */
    @Contract(pure = true)
    public static Eventu get() {
        return instance;
    }

    public Map<Class<?>, Boolean> getScanResultCache() {
        return scanResultCache;
    }

    /**
     * Forces a clear of the scan results cache.
     * Could be useful if classes changed during runtime. (What are you up to?!)
     */
    public void clearScanCache() {
        scanResultCache.clear();
    }

    public Map<Class<?>, List<Method>> getClassCache() {
        return classCache;
    }

    public static void fire(AbstractEvent event) {
        if(instance.executionHandler == null) {
            instance.executionQueue.add(event);
        } else {

            //get methodhandles that should be fired for this event
            //get instances for each method handle
            //fire event for each method handle for each instance

            List<MethodHandle> foundMethodHandles = Eventu.instance.repository.getMethodHandlesForEvent(event.getClass());
            for(MethodHandle handle : foundMethodHandles) {

                List<WeakReference> instances = instance.repository.getInstancesForMethodHandle(handle);
                List<WeakReference> unallocatedReferences = new ArrayList<>();

                for(WeakReference<?> instanceRef : instances) {
                    if(instanceRef.get() != null) {
                        instance.executionHandler.fireEvent(instanceRef.get(), handle, event);
                    } else {
                        unallocatedReferences.add(instanceRef);
                    }
                }

                unallocatedReferences.forEach(instances::remove);
            }
        }
    }
}
