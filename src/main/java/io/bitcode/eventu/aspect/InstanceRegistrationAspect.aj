package io.bitcode.eventu.aspect;

import io.bitcode.eventu.Eventu;
import io.bitcode.eventu.annot.EventHandler;
import org.aspectj.lang.JoinPoint;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public aspect InstanceRegistrationAspect {

    after(): execution(*.new(..))
            && !within(io.bitcode.eventu.aspect.InstanceRegistrationAspect)
            && !this(io.bitcode.eventu.AbstractEvent)
            && !this(io.bitcode.eventu.Eventu)
            && !this(io.bitcode.eventu.ClassRepository)
            {
//        long start = System.currentTimeMillis();

                Map<Class<?>, Boolean> scanResultCache = Eventu.get().getScanResultCache();
                Map<Class<?>, List<Method>> classCache = Eventu.get().getClassCache();

                if (scanResultCache.containsKey(thisJoinPoint.getClass())) {
                    if (scanResultCache.get(thisJoinPoint.getClass())) {
                        register(thisJoinPoint);
                    }
                } else {
                    List<Method> annotatedMethods = new ArrayList<>();

                    boolean shouldBeRegistered = false;
                    for (Method method : thisJoinPoint.getThis().getClass().getDeclaredMethods()) {
                        if (method.isAnnotationPresent(EventHandler.class)) {
                            shouldBeRegistered = true;
                            annotatedMethods.add(method);
                        }
                    }

                    scanResultCache.put(thisJoinPoint.getThis().getClass(), shouldBeRegistered);
                    if (shouldBeRegistered) {
                        classCache.put(thisJoinPoint.getThis().getClass(), annotatedMethods);
                        register(thisJoinPoint);
                    }
                }
//        System.out.println("Took " + (System.currentTimeMillis() - start) + " ms");
            }

    private void register(JoinPoint thisJoinPoint) {
        Object t = thisJoinPoint.getThis();
        if (!Eventu.isRegistered(t)) {
            Eventu.register(t);
        }
//        System.out.println("Registering " + thisJoinPoint.getThis().getClass().getSimpleName() + " because it has event handlers");
    }
}
