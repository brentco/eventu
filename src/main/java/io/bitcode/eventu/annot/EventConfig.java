package io.bitcode.eventu.annot;

public @interface EventConfig {
    boolean registerOnce() default false;
}
