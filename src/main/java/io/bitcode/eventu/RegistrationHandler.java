package io.bitcode.eventu;

public interface RegistrationHandler {

    void handleRegistration(ClassRepository repository, Object instance);
    boolean isRegistered(ClassRepository repository, Object instance);
}
