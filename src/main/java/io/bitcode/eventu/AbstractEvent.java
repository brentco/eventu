package io.bitcode.eventu;

public interface AbstractEvent {
    void cancel();
    boolean isCancelled();
}
