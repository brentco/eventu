package io.bitcode.eventu;

import io.bitcode.eventu.annot.EventHandler;
import io.bitcode.eventu.impl.BasicEvent;

public class EventTest {

    public static void main(String[] args) {
        EventTest eventTest;
        eventTest = new EventTest();
        eventTest = new EventTest();
        eventTest = new EventTest();
        eventTest = new EventTest();

        Eventu.initialize();

        Eventu.fire(new EventBla("Hey"));
    }


    @EventHandler
    private void catchEvent(EventBla event) {
        System.out.println("Received event " + event.getSup());
    }

    public static class EventBla extends BasicEvent {
        private String sup;

        public EventBla(String sup) {
            this.sup = sup;
        }

        public String getSup() {
            return sup;
        }
    }
}